version: '3'

services:
  # Database
  db:
    hostname: db
    image: postgres:9.6
    restart: always
    env_file: .env
    volumes:
      - ./postgres-data:/var/lib/posrgresql/data
    networks:
      - webnet

  # Docker Manager UI
  portainer:
    image: portainer/portainer
    ports:
      - "9000:9000"
    command: -H unix:///var/run/docker.sock
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
      - "portainer_data:/data"

  # RabbitMQ
  rabbit:
    hostname: rabbit
    image: rabbitmq:3.6-alpine
    restart: always
    env_file: .env
    depends_on:
      - db
    networks:
      - webnet

  # Elastic Search
  elasticsearch:
    hostname: elasticsearch
    image: elasticsearch:5.6-alpine
    restart: always
    volumes:
      - "es-data:/usr/share/elasticsearch/data"
    depends_on:
      - db
    networks:
      - webnet

  # Django application
  app:
    build: ./django/
    restart: always
    # Check README for wait-for-it.sh
    command: ['./scripts/wait-for-it.sh', './scripts/run_app_development.sh']
    volumes:
      - "./django/scripts:/scripts"
      - "./django/app:/code/app"
      - "static:/static"
      - "./media:/media"
      - "./logs:/logs"
    depends_on:
      - db
      - rabbit
      - elasticsearch
    env_file: .env
    environment:
      PYTHONUNBUFFERED: 1
      WAIT_FOR: db:5432,rabbit:5672
    networks:
      - webnet

  # Nginx Server
  nginx:
    restart: always
    image: nginx:1.15.5-alpine
    ports:
      - "80:80"
    volumes:
      - "./nginx/conf.d:/etc/nginx/conf.d"
      - "static:/usr/share/nginx/html/static"
      - "./media:/usr/share/nginx/html/media"
    depends_on:
      - app
    networks:
      - webnet

networks:
  webnet:
    driver: bridge

volumes:
  static:
  es-data:
  portainer_data:
