from django.contrib.auth import login
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, PasswordResetForm
from django.shortcuts import redirect
from django.views.generic.edit import FormView


class RegisterView(FormView):
    form_class = UserCreationForm
    template_name = 'register.html'

    def form_valid(self, form):
        user = form.save()
        # If submitted information is correct the registration is complete.

        # To login in new registration and redirect to dashboard use this.
        # login(self.request, user)
        return redirect('accounts.login')


class LoginView(FormView):
    form_class = AuthenticationForm
    template_name = 'login.html'

    def form_valid(self, form):
        login(self.request, form.get_user())
        return redirect('public.home')


class PasswordResetView(FormView):
    form_class = PasswordResetForm
    template_name = 'reset_password.html'

    def form_valid(self, form):
        form.save()
        return redirect('accounts.login')
