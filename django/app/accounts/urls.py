from django.urls import path
from accounts import views

urlpatterns = [
    path('login/', views.LoginView.as_view(), name='accounts.login'),
    path('register/', views.RegisterView.as_view(), name='accounts.register'),
    path('reset/password/', views.PasswordResetView.as_view(), name='accounts.reset_password'),
]
