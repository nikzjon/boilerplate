# Django Application

This is the django application powering packman project.

### Apps
Apps used in this project

1. **application** :  django root, houses all django settings and configs.
2. **public** : Public app used to serve public pages home page etc.
3. **app_auth** : Authentication and Pre Authentication tasks like Registration, Login, Password reset.
