# Application
This container wraps all the functions and scripts used by django application

### Docker Container
Whats inside the docker container.

Base Image :  `python:3.7.1-alpine3.8`

Packages : 
1. postgresql-dev 
2. gcc 
3. python-dev 
4. build-base 
5. py-pip 
6. jpeg-dev 
7. zlib-dev 
8. PIP packages specified in requirements.txt


### File structure
```
.
+-- scripts         # Contains bash scripts for handling various activities
+-- Application 	# Houses django and django apps
```

### Scripts 
To maintain consistency across different platforms the execution commands are grouped into shell scrpts inside `scripts` directory.

When docker container is created these scripts are copied into the machine and given execute permission.

### Note
For development the application directory inside the container is mounted to application directory in the system to 
reflect changes to code immediately.


## How to Run

Packman is ready to deploy application that means you can directly push this to production. But you definitely won't be doing it without some dev runs, here is how its done.

### Development
To run in development env:
1. Install Docker
2. From root of packman execute `docker-compose up` or `docker-compose up -d` to run as deamon.
3. The application directory ( directory with django ) is mounted directly to the docker machine running django for
	ease of development, so any changes made to code on system will be reflected in the docker vm.
	
	If you make changes to any other directory make sure to build image again. 'docker-compose build'

Docker files are optimized for caching.

#### Super user
1. To create super used make sure the docker containers are running.
2. Login to docker machine of application 'docker-compose exec app sh'
3. Navigate to application directory in VM. `cd ./application`
4. execute `./manage.py createsuperuser`
